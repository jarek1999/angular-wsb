import { Component, Input, OnInit } from '@angular/core';
import { Album } from 'src/app/core/model/Search';

@Component({
  selector: 'app-album-grid',
  templateUrl: './album-grid.component.html',
  styleUrls: ['./album-grid.component.scss']
})
export class AlbumGridComponent implements OnInit {

  @Input() albums: Album[] = [];
  
  constructor() { }

  ngOnInit(): void {
  }

}
