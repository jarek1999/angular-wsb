import { NgForOfContext } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from "../../../core/model/Playlist";

@Component({
  selector: 'app-playlists-list',
  templateUrl: './playlists-list.component.html',
  styleUrls: ['./playlists-list.component.scss'],
  // inputs: [
  //   'playlists:items'
  // ]
})
export class PlaylistsListComponent implements OnInit {

  @Input('items') playlists: Playlist[] = []

  // @Output('selectedIdChange') selectedIdChange = new EventEmitter<string>();
  @Output() selectedIdChange = new EventEmitter<string>();

  @Input() selectedId? = ''

  constructor() { }

  ngOnInit(): void {
  }

  select(playlist_id: string) {
    // this.selectedId = playlist_id

    this.selectedIdChange.emit(playlist_id)
  }

}
