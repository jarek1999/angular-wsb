## GIT 
cd ..
git clone https://bitbucket.org/ev45ive/angular-wsb.git angular-wsb
cd angular-wsb
npm i 
npm start

## GIT UPDATE
<!-- Stash my changes -->
git stash -u 
<!-- Pull newest version -->
git pull -f
npm i

## Instalacje
node -v 
v14.16.1

npm -v 
6.14.6

git --version
git version 2.31.1.windows.1

code -v
1.56.2

## VsCode
https://marketplace.visualstudio.com/items?itemName=Angular.ng-template

https://marketplace.visualstudio.com/items?itemName=Mikael.Angular-BeastCode

https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype

https://docs.emmet.io/cheat-sheet/

## Angular CLI (ng-cli - command line interface)
npm install -g @angular/cli
<!-- warnings are OK -->
<!-- C:\Users\<USER>\AppData\Roaming\npm\ng  -->
<!-- PATH -->
<!-- ng.cmd --version  /// Powershell -->
<!-- Git-bash with vscode https://medium.com/danielpadua/git-bash-with-vscode-593d5998f6be  -->
ng --version
Angular CLI: 12.0.1
ng help

# Generate new project
ng new --help
ng new angular-wsb --directory . --routing --strict --style scss

# Start project
ng serve
ng s -o 

## Bootstrap CSS
ng add @ng-bootstrap/ng-bootstrap
npm i bootstrap

## Playlists Screen:

ng g m playlists --routing -m app
ng g c playlists/container/playlists-view
ng g c playlists/components/playlists-list
ng g c playlists/components/playlist-details
ng g c playlists/components/playlist-edit-form


## Shared module - add to many modules
ng g m shared -m playlists

## Core module - added only once
ng g m core -m app

## Music Search
ng g m music-search -m app --routing 
ng g c music-search/container/album-search-view
ng g c music-search/container/album-details-view
ng g c music-search/components/search-form
ng g c music-search/components/album-grid
ng g c music-search/components/album-card
ng g i core/model/Search

ng g s core/services/music-search

## OAuth 2.0
https://manfredsteyer.github.io/angular-oauth2-oidc/docs/index.html

npm i angular-oauth2-oidc --save

ng g s core/services/auth

## Build
ng build 
ng build --prod